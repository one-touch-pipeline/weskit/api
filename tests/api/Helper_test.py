# SPDX-FileCopyrightText: 2024 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from pathlib import Path
import uuid

import tempfile

from weskit.classes.ProcessingStage import ProcessingStage
from weskit.api.Helper import run_log, read_wf_schema
from weskit.classes.Run import Run

mock_run_data = {
    "id": uuid.uuid4(),
    "request": {
        "workflow_url": "",
        "workflow_params": '{"text":"hello_world"}'
    },
    "processing_stage": ProcessingStage.RUN_CREATED,
    "request_time": None,
    "identity": "test_id",
    "exit_code": None,
    "sub_dir": None
}


def test_submission_worker():
    mock_run = Run(**mock_run_data)

    exec_log = run_log(mock_run)
    assert exec_log.get("request") == mock_run.request
    assert exec_log.get("state") == 'INITIALIZING'

    run_log_dict = exec_log.get("run_log", {})
    assert run_log_dict.get("cmd") is None
    assert run_log_dict.get("exit_code") is None


def test_wfinfo():
    result1: dict = read_wf_schema(Path("tests/workflow_schema.yaml"))
    assert len(result1) > 0

    result2: dict = read_wf_schema(Path("this/path/doesnt/exist.yaml"))
    assert len(result2) == 0

    with tempfile.NamedTemporaryFile(mode="w+", delete=True) as invalid_schema_file:
        invalid_schema_file.write('wrong_name:\n  "value"')
        invalid_schema_file.flush()
        result3: dict = read_wf_schema(Path(invalid_schema_file.name))
        assert len(result3) == 0

    with tempfile.NamedTemporaryFile(mode="w+", delete=True) as empty_schema_file:
        empty_schema_file.write("")
        empty_schema_file.flush()
        result4: dict = read_wf_schema(Path(empty_schema_file.name))
        assert len(result4) == 0
