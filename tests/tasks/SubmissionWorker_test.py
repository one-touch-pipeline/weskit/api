# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from pathlib import Path

import pytest
from abc import ABCMeta
import uuid
from datetime import datetime
from werkzeug.utils import cached_property

from weskit import PathContext
from weskit.classes.ShellCommand import ShellCommand, ss
from weskit.classes.executor2.Executor import Settings
from weskit.tasks.SubmissionWorker import (run_command_impl,
                                           addID_to_run, submit_run)
from weskit.classes.executor2.ProcessId import WESkitExecutionId
from weskit.classes.Run import Run
from weskit.tasks.SubmissionWorker import CommandTask
from weskit.classes.ProcessingStage import ProcessingStage

from tests.classes.executor2.MockExecutor_test import MockExecutor
from tests.classes.Database_test import MockDatabase


mock_run_data = {
    "id": uuid.uuid4(),
    "processing_stage": ProcessingStage.RUN_CREATED,
    "request_time": None,
    "identity": "test_id",
    "request": {
        "workflow_url": "",
        "workflow_params": '{"text":"hello_world"}'
    },
    "exit_code": None,
    "sub_dir": None
}


class CommandTaskMock(CommandTask, metaclass=ABCMeta):
    """
    Mock implementation of CommandTask.
    """

    @cached_property
    def executor(self):
        # Mocked executor for testing
        return MockExecutor(id=mock_run_data["id"])

    @cached_property
    def database(self):
        # Mocked database connection for testing
        return MockDatabase()


@pytest.mark.asyncio
async def test_submission_worker(temporary_dir, test_config):

    task = CommandTaskMock()

    run = Run(**mock_run_data)
    task.database.insert_run(run)

    # ID is missing in run
    assert run.current_execution_id is None

    command = ["echo", "hello world", ss(">"), "x"]
    context = PathContext(data_dir=Path(temporary_dir).parent,
                          workflows_dir=Path(temporary_dir),
                          singularity_containers_dir=Path(temporary_dir))
    workdir = Path(temporary_dir)
    command_obj = ShellCommand(command=command, workdir=workdir)

    # Run the command
    # WESkitExecutionId() will be added to the run as well
    await run_command_impl(
        task,
        command=command_obj,
        execution_settings=Settings(),
        worker_context=context,
        executor_context=context,
        run_id=run.id
        )

    run = task.database.get_run(run.id)
    assert run.id == mock_run_data["id"]
    assert isinstance(run.current_execution_id, WESkitExecutionId)
    assert run.processing_stage == ProcessingStage.RUN_CREATED

    # check if WESkitExecutionId() exists
    id = run.current_execution_id
    with pytest.raises(ValueError):
        run = await addID_to_run(task, run.id, id)

    # Submit the run to the executor
    # new WESkitExecutionId() created
    state = await submit_run(task,
                             run.id,
                             shell_command=command_obj,
                             executor_context=context,
                             execution_settings=context,
                             start_time=datetime.now(),
                             workdir=workdir)
    assert state.is_terminal is False
    assert state.lifetime is not None
    assert state.execution_id == run.current_execution_id
