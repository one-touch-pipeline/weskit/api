# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import pytest
from unittest.mock import patch, MagicMock, AsyncMock
from pathlib import Path
import asyncssh
from contextlib import suppress
from tenacity import stop_after_attempt

from weskit.classes.RetryableSshConnection import RetryableSshConnection
from weskit.classes.executor.ExecutorError import ConnectionError


@pytest.fixture
def ssh_connection():
    return RetryableSshConnection(
        username='testuser',
        host='testhost',
        keyfile=Path('/path/to/keyfile'),
        keyfile_passphrase='testpassphrase',
        knownhosts_file=Path('/path/to/knownhosts'),
        keepalive_interval="30s",
        keepalive_count_max=10,
        connection_timeout="10s"
    )


# asyncssh.read_private_key_list` is set to `MagicMock` in order the test to work properly.
# Otherwise, it would try to read from `keyfile=Path('/path/to/keyfile'),` which does not exists.
@pytest.mark.asyncio
async def test_connect(ssh_connection: RetryableSshConnection):
    with patch('asyncssh.connect', new_callable=AsyncMock) as mock_connect, \
         patch('asyncssh.read_private_key_list', new_callable=MagicMock) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]
        assert mock_connect.call_count == 0
        await ssh_connection.connect()
        assert mock_connect.call_count == 1


@pytest.mark.asyncio
async def test_connect_disconnect_error(ssh_connection: RetryableSshConnection):
    with patch('asyncssh.connect', new_callable=AsyncMock) as mock_connect, \
         patch('asyncssh.read_private_key_list', new_callable=MagicMock) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]
        mock_connect.side_effect = asyncssh.DisconnectError(code=0, reason='test')
        with pytest.raises(asyncssh.DisconnectError):
            await ssh_connection.connect()


@pytest.mark.asyncio
async def test_reconnect_after_lost_connection(ssh_connection: RetryableSshConnection):
    with patch('asyncssh.connect', new_callable=AsyncMock) as mock_connect, \
         patch('asyncssh.read_private_key_list', new_callable=MagicMock) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]

        # The side_effect simulates multiple connection losses
        mock_connect.side_effect = [
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            AsyncMock()  # Third call simulates a successful connection
        ]

        # Attempt to connect
        with suppress(ConnectionError):
            await ssh_connection.connect()

        # Verify that the retry logic was triggered and the connection was attempted 3 times
        assert mock_connect.call_count == 3  # 2 failed attempts + 1 successful connection


@pytest.mark.asyncio
async def test_run_after_lost_connection(ssh_connection: RetryableSshConnection):
    with patch("asyncssh.connect", new_callable=AsyncMock) as mock_connect, \
         patch("asyncssh.read_private_key_list", new_callable=MagicMock) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]

        # The side_effect simulates multiple connection losses
        mock_connect.return_value.run.side_effect = [
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            AsyncMock(),
        ]

        await ssh_connection.connect()
        await ssh_connection.run("fake command")
        assert mock_connect.return_value.run.call_count == 3


@pytest.mark.asyncio
async def test_create_process_after_lost_connection(
    ssh_connection: RetryableSshConnection,
):
    with patch("asyncssh.connect", new_callable=AsyncMock) as mock_connect, patch(
        "asyncssh.read_private_key_list", new_callable=MagicMock
    ) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]
        # The side_effect simulates multiple connection losses
        mock_connect.return_value.create_process.side_effect = [
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            AsyncMock(),
        ]
        await ssh_connection.connect()
        await ssh_connection.create_process("fake command")
        assert mock_connect.return_value.create_process.call_count == 3


@pytest.mark.asyncio
async def test_retry_option_arguments(
    ssh_connection: RetryableSshConnection,
):
    with patch("asyncssh.connect", new_callable=AsyncMock) as mock_connect, patch(
        "asyncssh.read_private_key_list", new_callable=MagicMock
    ) as mock_read_key_list:
        mock_read_key_list.return_value = [MagicMock()]
        # The side_effect simulates multiple connection losses
        mock_connect.return_value.run.side_effect = [
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            asyncssh.misc.ConnectionLost("Mocked connection lost"),
            AsyncMock(),
        ]

        retry_options = {"stop": stop_after_attempt(2)}
        await ssh_connection.connect()
        with suppress(asyncssh.misc.ConnectionLost):
            await ssh_connection.run("fake command", retry_options=retry_options)
        assert mock_connect.return_value.run.call_count == 2
