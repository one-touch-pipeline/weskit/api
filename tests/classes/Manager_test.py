# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import json
import os
import shutil
import pytest
from tests.test_utils import get_mock_run, get_workflow_data


@pytest.mark.integration
def test_manager_insert_tags(testOidcIdentity,
                             manager):
    tags = json.dumps(["tag1", "tag2"])
    request = get_workflow_data(
        snakefile="file:tests/wf1/Snakefile",
        config="tests/wf1/config.yaml",
        engine_params={
            "max-memory": "150m",
            "max-runtime": "00:01:00",
            "accounting-name": "projectX",
            "job-name": "testjob",
            "group": "testgroup",
            "queue": "testqueue"
        })
    request["tags"] = tags
    run = manager.create_and_insert_run(validated_request=request,
                                        identity=testOidcIdentity.oauth_sub_id)
    assert run.request["tags"] == json.loads(tags)


@pytest.mark.integration
def test_manager_subdir_missing(manager,
                                celery_worker):
    engine_env_path = manager.executor_context.workflows_dir.absolute() / "wf1" / "env.sh"
    run = get_mock_run(workflow_url="file:wf1/Snakefile",
                       workflow_type="SMK",
                       workflow_type_version="7.30.2",
                       workflow_engine_parameters={
                           "engine-environment": str(engine_env_path)
                       })
    run.sub_dir = None
    with pytest.raises(RuntimeError):
        run = manager._process_workflow_attachment(run, files=[])


@pytest.mark.integration
def test_manager_trsworkflow(manager):
    trs_base_uri = 'trs://dev.workflowhub.eu:443'
    trs_path = '/230/1/SMK/workflow/Snakefile'
    engine_env_path = manager.executor_context.workflows_dir.absolute() / "wf1" / "env.sh"
    run = get_mock_run(workflow_url=trs_base_uri + trs_path,
                       workflow_type="SMK",
                       workflow_type_version="7.30.2",
                       workflow_engine_parameters={
                           "engine-environment": str(engine_env_path)
                       })
    run = manager.prepare_execution(run, files=[])
    assert run.rundir_rel_workflow_path is not None
    if os.path.exists("tests/230"):
        shutil.rmtree("tests/230")
