# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from pathlib import Path
from datetime import datetime, timedelta
from typing import List, TypeVar, Optional, Dict
from time import sleep
from signal import Signals

from urllib3.util.url import Url

from weskit.classes.ShellCommand import ShellCommand
from weskit.classes.executor2.State import State
from weskit.classes.executor2.ProcessId import WESkitExecutionId, Identifier
from weskit.classes.executor2.Executor import Executor, Settings, Result
from weskit.classes.storage.LocalStorageAccessor import LocalStorageAccessor

S = TypeVar("S")


class MockState(State[str]):

    def __init__(self, execution_id: WESkitExecutionId, created_at: Optional[datetime] = None):
        super().__init__(execution_id, created_at)

    @property
    def is_terminal(self) -> bool:
        return False

    @property
    def lifetime(self) -> timedelta:
        return datetime.now() - self.created_at


class MockExecutor(Executor[int]):
    def __init__(self, id: Identifier[str], log_dir_base: Optional[Path] = None):
        super().__init__(id, log_dir_base)
        self.execution_states: Dict[WESkitExecutionId, State] = {}

    @property
    def hostname(self) -> str:
        return "MockExecutor"

    async def execute(self,
                      execution_id: WESkitExecutionId,
                      command: ShellCommand,
                      stdout_file: Optional[Url] = None,
                      stderr_file: Optional[Url] = None,
                      stdin_file: Optional[Url] = None,
                      settings: Optional[Settings] = None,
                      **kwargs) -> State[int]:

        state: State = MockState(execution_id, created_at=datetime.now())
        self.execution_states[execution_id] = state
        return state

    async def get_status(self,
                         execution_id: WESkitExecutionId,
                         log_dir: Optional[Url] = None,
                         pid: Optional[int] = None) -> Optional[State[int]]:
        return self.execution_states.get(execution_id)

    async def update_status(self,
                            current_state: State[int],
                            log_dir: Optional[Url] = None,
                            pid: Optional[int] = None) -> Optional[State[int]]:
        return self.execution_states.get(current_state.execution_id)

    async def get_result(
        self, state: State[S], shell_command: ShellCommand, execution_log: Dict
    ) -> Result[S]:
        raise NotImplementedError

    async def kill(self,
                   state: State[int],
                   signal: Signals) -> bool:
        # Send killing signal to process
        return True

    async def wait(self, state: State[int]) -> None:
        # Waiting by sleeping for a short time
        sleep(0.01)

    @property
    def storage(self) -> LocalStorageAccessor:
        mock_accessor = LocalStorageAccessor()
        return mock_accessor

    async def get_states(
        self, execution_ids: List[WESkitExecutionId]
    ) -> List[State[S]]:
        raise NotImplementedError

    async def update_states(self, current_run_states: List[State[S]]) -> List[State[S]]:
        raise NotImplementedError
