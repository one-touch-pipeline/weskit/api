# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import pytest
from datetime import timedelta
from pathlib import Path
from urllib3.util.url import parse_url
from datetime import datetime
from signal import Signals
from kubernetes.client.exceptions import ApiException
from weskit.classes.executor2.State import (
    NonTerminalState,
    TerminalState,
    Pending,
    Running,
    Start,
    Succeeded,
    Failed,
)
from weskit.classes.executor2.Executor import (
    Settings,
)
from weskit.classes.executor2.kubernetes.KubernetesPhases import (
    KubernetesPhases,
)
from weskit.classes.executor2.ProcessId import ProcessId, WESkitExecutionId
from weskit.memory_units import Memory, Unit
from weskit.classes.ShellCommand import ShellCommand
from unittest.mock import patch


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_execute(kubernetes_executor):
    """
    Ensure that the folder /localVolumes/api/test_log exists before running
    the test on a local system.
    """
    EXECUTION_ID = WESkitExecutionId()
    execution_state = await kubernetes_executor.execute(
        execution_id=EXECUTION_ID,
        command=ShellCommand(
            workdir=Path("."),
            command=[
                "snakemake",
                "-s",
                "/workflows/wf2/Snakefile",
                "--configfile",
                "/workflows/wf2/config.yaml",
                "--cores",
                "1",
            ],
        ),
        stdout_file=parse_url("".join(["file://",
                              kubernetes_executor.log_dir_base,
                              "stdout"])),
        stderr_file=parse_url("".join(["file://",
                              kubernetes_executor.log_dir_base,
                              "stderr"])),
        stdin_file=parse_url("file:///dev/null"),
        settings=Settings(
            max_retries=1,
            cores=1,
            walltime=timedelta(minutes=5.0),
            memory=Memory(100, Unit.MEGA),
            container_image="snakemake/snakemake",
        ),
    )
    assert str(Pending.name) == str(execution_state.name)


@pytest.mark.asyncio
@pytest.mark.kubernetes
@pytest.mark.parametrize(
    "expected_execution_state,"
    "previous_phase,"
    "previous_state,"
    "current_state,"
    "pod_phase",
    [
        (Running, "Pending", Start, Pending, "Running"),
        (Succeeded, "Running", Pending, Running, "Succeeded"),
        (Failed, "Running", Pending, Running, "Failed"),
    ],
)
async def test_update_status(
    expected_execution_state,
    previous_phase,
    previous_state,
    current_state,
    pod_phase,
    kubernetes_executor,
):
    processID = ProcessId(12234, "kubernetes")
    previous_foreign_state = KubernetesPhases.as_foreign_state(
        pid=processID, name=previous_phase
    )
    current_state = current_state(
        execution_id=WESkitExecutionId(),
        foreign_state=previous_foreign_state,
        previous_state=previous_state,
    )
    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name", return_value=pod_phase
    ):
        execution_state = await kubernetes_executor.update_status(
            current_state=current_state
        )
        assert str(execution_state.name) == str(expected_execution_state.name)


@pytest.mark.asyncio
@pytest.mark.kubernetes
@pytest.mark.parametrize(
    "expected_execution_state,"
    "previous_phase,"
    "previous_state,"
    "current_state,"
    "pod_phase",
    [
        (Running, "Pending", Start, Pending, "Running"),
        (Succeeded, "Running", Pending, Running, "Succeeded"),
        (Failed, "Running", Pending, Running, "Failed"),
    ],
)
async def test_get_status(
    expected_execution_state,
    previous_phase,
    previous_state,
    current_state,
    pod_phase,
    kubernetes_executor,
):
    processID = ProcessId(12234, "kubernetes")
    previous_foreign_state = KubernetesPhases.as_foreign_state(
        pid=processID, name=previous_phase
    )
    execution_id = WESkitExecutionId()
    current_state = current_state(
        execution_id=execution_id,
        foreign_state=previous_foreign_state,
        previous_state=previous_state,
    )
    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name", return_value=pod_phase
    ):
        execution_state = await kubernetes_executor.get_status(
            execution_id=execution_id
        )
        assert str(execution_state.name) == str(expected_execution_state.name)


@pytest.mark.asyncio
@pytest.mark.kubernetes
@pytest.mark.parametrize(
    "job_name," "container_image,",
    [
        (WESkitExecutionId(), "snakemake/snakemake"),
    ],
)
async def test_generate_job(job_name, container_image, kubernetes_executor):
    # TODO: Add a test to check if creating a job for Nextflow works fine
    command = ShellCommand(
        workdir=Path("."),
        command=[
            "snakemake",
            "-s",
            "/workflows/wf2/Snakefile",
            "--configfile",
            "/workflows/wf2/config.yaml",
            "--cores",
            "1",
        ],
    )
    job = await kubernetes_executor._generate_job(
        job_name=str(job_name.value).lower(),
        container_image=container_image,
        command=command,
    )
    assert str(job_name.value).lower() == job.metadata.name


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_get_result_succeeded(kubernetes_executor):
    execution_id = WESkitExecutionId()
    shell_command = ShellCommand(
        workdir=Path("."),
        command=[
            "snakemake",
            "-s",
            "/workflows/wf2/Snakefile",
            "--configfile",
            "/workflows/wf2/config.yaml",
            "--cores",
            "1",
        ],
    )
    execution_log = {
        "stdout_file": "file:///api/test_log/stdout",
        "stderr_file": "file:///api/test_log/stderr",
        "start_time": datetime.now(),
        "exit_code": None,
    }
    # simulate the pod phase as Succeeded
    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name", return_value="Succeeded"
    ):
        state = Pending(execution_id, KubernetesPhases.as_foreign_state(
            pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
            name="Running",
        ), "Pending")
        result = await kubernetes_executor.get_result(state, shell_command, execution_log)
        assert result.stdout_url.scheme == execution_log["stdout_file"]
        assert result.end_time is not None
        assert result.command == shell_command


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_get_result_failed(kubernetes_executor):
    execution_id = WESkitExecutionId()
    shell_command = ShellCommand(
        workdir=Path("."),
        command=[
            "snakemake",
            "-s",
            "/workflows/wf2/Snakefile",
            "--configfile",
            "/workflows/wf2/config.yaml",
            "--cores",
            "1",
        ],
    )
    stdout_file = "file://" + str(kubernetes_executor.log_dir_base) + "/stdout"
    stderr_file = "file://" + str(kubernetes_executor.log_dir_base) + "/stderr"
    execution_log = {
        "stdout_file": stdout_file,
        "stderr_file": stderr_file,
        "start_time": datetime.now(),
        "exit_code": None,
    }
    # simulate the pod phase as Failed
    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name", return_value="Failed"
    ):
        state = Pending(execution_id, KubernetesPhases.as_foreign_state(
            pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
            name="Running",
        ), "Pending")
        result = await kubernetes_executor.get_result(state, shell_command, execution_log)
        assert result.stdout_url.scheme == execution_log["stdout_file"].lower()
        assert result.end_time is not None
        assert result.command == shell_command


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_wait_succeeded(kubernetes_executor):
    execution_id = WESkitExecutionId()
    state = Pending(execution_id, KubernetesPhases.as_foreign_state(
        pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
        name="Pending",
    ), "Pending")

    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name",
        side_effect=["Pending", "Running", "Succeeded"]
    ):
        await kubernetes_executor.wait(state)


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_wait_failed(kubernetes_executor):
    execution_id = WESkitExecutionId()
    state = Pending(execution_id, KubernetesPhases.as_foreign_state(
        pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
        name="Pending",
    ), "Pending")

    with patch.object(
        kubernetes_executor, "get_pod_phase_by_job_name",
        side_effect=["Pending", "Running", "Failed"]
    ):
        await kubernetes_executor.wait(state)


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_kill_job_exists(kubernetes_executor):
    execution_id = WESkitExecutionId()
    state = Pending(execution_id, KubernetesPhases.as_foreign_state(
        pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
        name="Running",
    ), "Pending")

    with patch.object(
        kubernetes_executor.k8s_batch_v1_client, "delete_namespaced_job", return_value=True
    ) as mock_delete_job:
        success = await kubernetes_executor.kill(state, Signals.SIGTERM)
        mock_delete_job.assert_called_once_with(
            name=str(execution_id.value).lower(),
            namespace=kubernetes_executor.namespace,
            body={'propagationPolicy': 'Foreground'}
        )
        assert success is True


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_kill_job_not_exists(kubernetes_executor):
    execution_id = WESkitExecutionId()
    state = Pending(execution_id, KubernetesPhases.as_foreign_state(
        pid=ProcessId(str(execution_id.value).lower(), "kubernetes"),
        name="Running",
    ), "Pending")

    with patch.object(
        kubernetes_executor.k8s_batch_v1_client, "delete_namespaced_job",
        side_effect=ApiException(status=404)
    ) as mock_delete_job:
        success = await kubernetes_executor.kill(state, Signals.SIGTERM)
        mock_delete_job.assert_called_once()
        assert success is False


@pytest.mark.asyncio
@pytest.mark.kubernetes
async def test_update_states(kubernetes_executor):

    execution_id_1 = WESkitExecutionId()
    execution_id_2 = WESkitExecutionId()

    # Mock data for Kubernetes foreign states
    mock_foreign_states = {
        execution_id_1: KubernetesPhases.as_foreign_state(
            pid=ProcessId("job1", "kubernetes"), name="Running"
        ),
        execution_id_2: KubernetesPhases.as_foreign_state(
            pid=ProcessId("job2", "kubernetes"), name="Succeeded"
        ),
    }

    non_terminal_state = Running(
        execution_id=execution_id_1,
        foreign_state=mock_foreign_states[execution_id_1],
        previous_state=Start(execution_id_1)
    )
    terminal_state = Succeeded(
        execution_id=execution_id_2,
        foreign_state=mock_foreign_states[execution_id_2],
        previous_state=Start(execution_id_2)
    )

    with patch.object(kubernetes_executor, "_get_states_dict", return_value=mock_foreign_states):
        with patch.object(kubernetes_executor, "_state_mapper") as mock_state_mapper:
            mock_state_mapper.side_effect = [non_terminal_state, terminal_state]

            updated_states = await kubernetes_executor.update_states([non_terminal_state,
                                                                      terminal_state])

            assert len(updated_states) == 2

            assert isinstance(updated_states[0], NonTerminalState)
            assert isinstance(updated_states[1], TerminalState)
            assert updated_states[0].foreign_states[0]._wrapped_state == KubernetesPhases.Running
            assert updated_states[1].foreign_states[0]._wrapped_state == KubernetesPhases.Succeeded
