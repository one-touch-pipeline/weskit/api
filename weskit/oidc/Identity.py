# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

from dataclasses import dataclass
from typing import Dict, Set, Any

from flask import Flask


not_logged_in_subject_id = "not-logged-in-subject"


@dataclass
class Identity:
    """
    This class makes the content of the JWT easily accessible.
    Note: There used to be more fields that just oauth_sub_id.
    We kept it, in case there would be more fields needed from jwt_payload.items()
    """
    oauth_sub_id: str = not_logged_in_subject_id

    @staticmethod
    def from_jwt_payload(app: Flask, jwt_payload: Dict[Any, Any]) -> Identity:
        """
        Create Identity object from jwt_payload dictionary. Only fields that can be processed are
        mapped into the Identity instance. Other fields are ignored.

        :param app: Flask application object
        :param jwt_payload: Dictionary with the JWT payload as received from the client.
        :return:
        """
        identity_claim = app.config["JWT_IDENTITY_CLAIM"]
        if identity_claim not in jwt_payload.keys():
            raise KeyError(f"JWT payload is missing required identity claim: {identity_claim}")

        key_mapping = {
            identity_claim: "oauth_sub_id"}

        allowed_fields: Set[str] = {identity_claim}
        cl = Identity(**{
            key_mapping.get(k, k): v
            for k, v in jwt_payload.items()
            if k in allowed_fields
        })
        return cl
