# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import logging
from functools import wraps
from typing import Callable

import requests
from flask import current_app as flask_current_app
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_jwt_extended.view_decorators import _decode_jwt_from_headers, verify_jwt_in_request
from weskit.classes.WESApp import WESApp
from weskit.utils import safe_getenv

logger = logging.getLogger(__name__)


def login_required():
    """
    This decorator checks whether the login is initialized. If not all endpoints are exposed
    unprotected. Otherwise, the decorator validates the access_token. If validateOnline==True
    the access_token will be validated by calling the identity provider. ValidateOnline==False
    will only check the certificate of the token offline. validateOnline should be true in
    high security cases that will cause changes in the backend. (cancelRun, submitRun)

    Compare: https://flask-jwt-extended.readthedocs.io/en/stable/custom_decorators/

    :param validate_online: bool should the identity provider asked for validity of the token?
    """

    def wrapper(fn: Callable):
        @wraps(fn)
        def decorator(*args, **kwargs):
            try:
                current_app = WESApp.from_current_app(flask_current_app)
                try:
                    if current_app.authentication_enabled:
                        # this function does no online-validation of the jwt
                        verify_jwt_in_request()
                except NoAuthorizationError:
                    return {"msg": "Unauthorized access"}, 401
                if current_app.authentication_enabled and not validate_online(current_app):
                    # Respond with unspecific "Identity validation failed" error. The client
                    # should not be informed that this is because of "online" validation,
                    # because this would give an attacker information about the system.
                    return {"msg": "Identity validation failed"}, 401
                return fn(*args, **kwargs)
            except Exception as e:
                # It is important, that we also log errors during this annotation code. The
                # @login_required annotation is used in all endpoints and would otherwise not
                # result in a meaningful stacktrace in the logs that could help us to diagnose
                logger.error(e, exc_info=e)
                raise e

        return decorator

    return wrapper


def validate_online(app) -> bool:
    """
    Checks the validity of a token with a request to OIDC provider.
    Returns True if token is valid.
    """

    try:
        token = _decode_jwt_from_headers()[0]
        j = requests.post(
            data={"token": token},
            url=app.oidc_config["introspection_endpoint"],
            # credetials are needed for the introspection endpoint
            auth=(safe_getenv('IDP_CLIENTID'), safe_getenv('IDP_CLIENT_SECRET')),
            timeout=60
        ).json()
    except Exception as e:
        logger.error("Could not reach OIDC provider for online validation", exc_info=e)
        return False

    if j.get('active', False):
        return True
    else:
        logger.warning(
            f"Online validation failed for client: {safe_getenv('IDP_CLIENTID')}"
        )
        return False
