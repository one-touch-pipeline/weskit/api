# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import logging
from time import sleep
from typing import Optional

import requests
from flask import Flask
from flask_jwt_extended import JWTManager

from weskit.classes.WESApp import WESApp
from weskit.oidc.Identity import Identity

logger = logging.getLogger(__name__)


def _authentication_enabled(config: dict) -> bool:
    """
    This function checks if all required configurations are set end enabled in the config file
    It returns true if the Login is correctly set up end enabled, otherwise false.

    :param config: configuration dict
    :return:
    """
    # Check whether the Login System can be initialized
    if ("login" in config and
            config['login'].get("enabled", False) and
            "jwt" in config['login']):
        return True

    else:
        logger.warning("Login System Disabled")
        logger.warning("has login block: {}, login is enabled: {}, has jwt: {}".
                       format("login" in config,
                              config['login'].get("enabled", False),
                              "jwt" in config['login']))
        return False


def setup(app: WESApp, config: dict) -> None:
    """
    Factory for setting up everything needed for OIDC authentication, including

       * validating environment variables and config
       * setting up environment variables
       * registering login_required annotation
       * configure Flask app
       * set up current_user (user_loader_callback)

    This makes multiple requests to the issuer/identity provider!
    """
    if not _authentication_enabled(config):
        app.authentication_enabled = False
        # flask-jwt-extension 4 needs some of these variables to be always set. Although the
        # documentation says for many of them that there is a default (e.g. for
        # JWT_TOKEN_LOCATION, JWT_HEADER_NAME, and JWT_HEADER_TYPE), we do get exceptions,
        # if the values are not set explicitly.
        _copy_jwt_vars_to_toplevel_config(app, config)
    else:
        app.authentication_enabled = True

        # copy weskit config to app config
        _copy_jwt_vars_to_toplevel_config(app, config)

        issuer_url = app.config['JWT_DECODE_ISSUER']

        if not issuer_url:
            raise KeyError("Issuer URL is missing or not set via JWT_DECODE_ISSUER")

        # get oidc config from IdP
        oidc_config = _retrieve_oidc_config(issuer_url)

        logger.info("Issuer URL and public key successfully retrieved")

        # Validate the issuer
        normalized_issuer_url = issuer_url.rstrip('/')
        normalized_oidc_issuer = oidc_config.get("issuer", "").rstrip('/')
        if normalized_issuer_url != normalized_oidc_issuer:
            raise RuntimeError("Issuer URL does not match the issuer URL" +
                               " in the OIDC configuration or is not available")

        jwt_manager = JWTManager(app)

        app.oidc_config = oidc_config

        @jwt_manager.user_lookup_loader
        def client_loader_callback(jwt_headers: dict, jwt_payload: dict) -> Identity:
            """
            This function returns a Identity object
            if the flask_jwt_extended current_user is called.

            :return: Identity
            """
            return Identity.from_jwt_payload(app, jwt_payload)


def _copy_jwt_vars_to_toplevel_config(flaskapp: Flask, config: dict) \
        -> None:
    """
    Flask_jwt_extended expect its configuration in the app.config object.
    Therefore, we copy the config to the app object.
    """
    jwt_config = config.get('login', {}).get('jwt', {})
    for key in jwt_config:
        flaskapp.config[key] = jwt_config.get(key, None)


def _retrieve_oidc_config(issuer_url: str, timeout_sec: int = 10) -> dict:
    config_url = "%s/.well-known/openid-configuration" % issuer_url
    oidc_config: Optional[dict] = None
    for retry in range(4, -1, -1):
        try:
            response = requests.get(config_url, timeout=60)
            oidc_config = _validate_issuer_config_response(response)
            break
        except Exception as e:
            logger.warning(f"Unable to receive JWKS endpoint from {config_url}." +
                           f" Retrying {retry} times")
            if not retry:
                logger.exception(
                    f"Timeout! Could not receive JWKS endpoint from '{config_url}'!"
                )
                raise TimeoutError(
                    f"Failed to retrieve OIDC configuration from {config_url} after retries"
                ) from e
            sleep(timeout_sec)

    if oidc_config is None:
        raise RuntimeError("Could not retrieve OIDC configuration")
    else:
        return oidc_config


def _validate_issuer_config_response(response) -> dict:
    """
    This function validates that the config received from the Identity Provider has the correct
    format and contains all required data.
    :param config: Anything returned from .json().
    :return:
    """
    config = response.json()

    if not isinstance(config, dict):
        raise ValueError("Identity provider didn't respond with a dict but '%s': %s" %
                         (str(type(config)), response))
    required_values = [
        'issuer',
        'introspection_endpoint',
        'token_endpoint',
        'jwks_uri'
    ]

    missing_values = [value for value in required_values if value not in config]
    if missing_values:
        raise ValueError("Missing fields in issuer response: %s" % ", ".join(missing_values))

    return config
