# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
import os
from abc import ABCMeta
from datetime import datetime
from typing import Union, TypeVar
from celery import Task
from pathlib import Path
from werkzeug.utils import cached_property
from uuid import UUID

from weskit.classes.Database import Database
from weskit.classes.Run import Run
from weskit.celery_app import celery_app, read_config, update_celery_config_from_env
from weskit.classes.PathContext import PathContext
from weskit.classes.ShellCommand import ShellCommand
from weskit.classes.executor2.State import State
from weskit.classes.executor2.Executor import Settings
from weskit.classes.executor2.EngineExecutor import get_executor
from weskit.classes.executor2.EngineExecutorType import EngineExecutorType
from weskit.classes.executor2.ProcessId import WESkitExecutionId
from weskit.utils import format_timestamp

logger = logging.getLogger(__name__)

S = TypeVar("S")


class CommandTask(Task, metaclass=ABCMeta):
    """
    Process-global state for all run_command tasks. Use this for sockets and network connections,
    etc. This allows sharing resources between tasks:

    - SSH connection
    - database connection

    See https://celery-safwan.readthedocs.io/en/latest/userguide/tasks.html#instantiation
    """

    @cached_property
    def config(self):
        config = read_config()
        update_celery_config_from_env()
        return config

    @cached_property
    def executor_type(self) -> EngineExecutorType:
        return EngineExecutorType.from_string(self.config["executor"]["type"])

    @cached_property
    def executor(self):
        logger.info("Determine executor for celery task")
        return get_executor(self.executor_type,
                            login_parameters=self.config["executor"]["login"]
                            if self.executor_type.needs_login_credentials
                            else None)

    @cached_property
    def database(self):
        logger.info("Get database connection for celery task")
        database_url = os.getenv("WESKIT_DATABASE_URL")
        return Database(database_url, "WES")


async def addID_to_run(task: Task, run_id: UUID, execution_id: WESkitExecutionId) -> Run:
    """ Add a WESkitExecutionId() to the run and update the database. """
    run: Run = task.database.get_run(run_id)
    if run.current_execution_id is None or run.current_execution_id != execution_id:
        run.current_execution_id = execution_id
    else:
        raise ValueError(
            f"WESkitExecutionId() already exists for run {run.current_execution_id}"
        )
    run = task.database.update_run(run, Run.merge, 1)
    return run


async def submit_run(task: Task,
                     run_id: UUID,
                     shell_command: ShellCommand,
                     executor_context: PathContext,
                     execution_settings: Settings,
                     start_time: datetime,
                     workdir: Path
                     ) -> State:
    """ Submit the run to the executor. """
    execution_id = WESkitExecutionId()
    run = await addID_to_run(task, run_id, execution_id)
    state = await task.executor.execute(
        execution_id=run.current_execution_id,
        command=shell_command,
        stdout_file=executor_context.stdout_file(workdir, start_time),
        stderr_file=executor_context.stderr_file(workdir, start_time),
        settings=execution_settings,
    )
    run = task.database.update_run(run, Run.merge, 1)

    return state


async def create_log_dir(task: Task):
    log_dir = task.executor.log_dir_base
    logger.info(f"Creating log-dir {log_dir}")
    storage = task.executor.storage
    await storage.create_dir(log_dir, exists_ok=True)


async def run_command_impl(task: Task,
                           command: ShellCommand,
                           execution_settings: Settings,
                           worker_context: PathContext,
                           executor_context: PathContext,
                           run_id: UUID
                           ):

    start_time = datetime.now()

    workdir = command.workdir

    if workdir is None:
        raise RuntimeError(f"No working directory defined for command: {command}")

    logger.info(
        f"Running command in {worker_context.run_dir(workdir)} (worker) = "
        f"{executor_context.run_dir(workdir)} (executor): {repr(command.command)}"
    )

    shell_command = ShellCommand(
        command=command.command,
        workdir=executor_context.workdir(workdir),
        environment=command.environment
    )

    try:
        # Create the log directory
        await create_log_dir(task)

        await submit_run(task,
                         run_id,
                         shell_command,
                         executor_context,
                         execution_settings,
                         start_time,
                         workdir)

        # The execution context is the run-directory. It is used to create all paths to be reported
        # relative to the run-directory.
        rundir_context = PathContext(Path("."), Path("."), Path("."))
        execution_log = {
            "start_time": format_timestamp(start_time),
            "cmd": [str(el) for el in command.command],
            "env": command.environment,
            "workdir": str(workdir),
            "end_time": None,
            "exit_code": None,
            "stdout_file": str(rundir_context.stdout_file(Path("."), start_time)),
            "stderr_file": str(rundir_context.stderr_file(Path("."), start_time)),
            "log_dir": str(rundir_context.log_dir(Path("."), start_time)),
            "log_file": str(rundir_context.execution_log_file(Path("."), start_time)),
            "output_files": None
        }
        run = task.database.get_run(run_id)
        run.execution_log = execution_log
        run = task.database.update_run(run, Run.merge, 1)
    except Exception as e:
        logger.error("Error during execution", exc_info=e)
        raise


@celery_app.task(base=CommandTask)
async def run_command(command: ShellCommand,
                      execution_settings: Settings,
                      worker_context: PathContext,
                      executor_context: PathContext,
                      run_id: Union[UUID, str]
                      ):

    await run_command.run_command_impl(command,
                                       execution_settings,
                                       worker_context,
                                       executor_context,
                                       run_id)
