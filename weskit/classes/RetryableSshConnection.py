# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import logging
from contextlib import asynccontextmanager
from pathlib import Path
from typing import Optional, Union, Callable, TypeVar, Awaitable

import asyncssh
from asyncssh import (
    ChannelOpenError,
    DisconnectError,
    SSHClientConnection,
    SSHClientProcess,
    SSHCompletedProcess,
)
from tenacity import (
    AsyncRetrying,
    after_log,
    retry_if_exception,
    stop_after_attempt,
    wait_exponential,
    wait_random,
)
from urllib3.util import Url

from weskit.classes.executor.ExecutorError import ExecutorError

T = TypeVar('T')

logger = logging.getLogger(__name__)


def is_connection_interrupted(exception: BaseException) -> bool:
    return isinstance(exception, (asyncssh.misc.ChannelOpenError,
                                  asyncssh.misc.ConnectionLost))


class RetryableSshConnection:
    """
    Just a thin convenience-wrapper around asyncssh and tenacity.
    """

    def __init__(self,
                 username: str,
                 host: str,
                 keyfile: Path,
                 keyfile_passphrase: str,
                 knownhosts_file: Path,
                 keepalive_interval: Optional[Union[int, float, str]],
                 keepalive_count_max: Optional[int],
                 connection_timeout: Optional[str] = "2m",
                 port: int = 22,
                 retry_options: Optional[dict] = None):
        """
        Keepalive: This is for a server application. The default is to have a keep-alive, but one
                   that allows recovery after even a long downtime of the remote host.

        :param username: Remote SSH username.
        :param host: Remote host.
        :param keyfile: Only key-based authentication is supported.
        :param keyfile_passphrase: The passphrase to use for the keyfile.
        :param knownhosts_file: Path to a knownHosts file to authenticate the server side.
        :param connection_timeout: The timeout for establishing a new connection.
        :param keepalive_interval: How frequently the connection state shall be checked.
        :param keepalive_count_max: The number of tries, in case the connection cannot be verified.
        :param port: Defaults to standard SSH-port 22.
        """
        self._username = username
        self._host = host
        self._connection_timeout = connection_timeout
        self._keepalive_interval = keepalive_interval
        self._keepalive_count_max = keepalive_count_max
        self._keyfile = keyfile
        self._keyfile_passphrase = keyfile_passphrase
        self._port = port
        self._knownhosts_file = knownhosts_file

        if retry_options is None:
            retry_options = {
                "wait_exponential": {
                    "multiplier": 1,
                    "min": 4,
                    "max": 300
                },
                "wait_random": {
                    "min": 0,
                    "max": 1
                },
                "stop_after_attempt": 5
            }
        self._retry_options = {
            # By default, do exponential backoff with jitter, to avoid synchronous reconnection
            # attempts by synchronously affected connections.
            "wait":
                wait_exponential(**retry_options["wait_exponential"]) +
                wait_random(**retry_options["wait_random"]),
            "stop":
                stop_after_attempt(retry_options["stop_after_attempt"]),
        }

        self._connection: Optional[SSHClientConnection] = None

    async def connect(self) -> None:
        # Some related docs:
        # * https://asyncssh.readthedocs.io/en/latest/api.html#specifying-known-hosts
        # * https://asyncssh.readthedocs.io/en/latest/api.html#specifying-private-keys
        # * https://asyncssh.readthedocs.io/en/latest/api.html#sshclientconnectionoptions
        ssh_keys = asyncssh.read_private_key_list(
            Path(self._keyfile),
            self._keyfile_passphrase)
        logger.info(f"Read private keys from {self._keyfile}. " +
                    f"sha256 fingerprints: {list(map(lambda k: k.get_fingerprint(), ssh_keys))}")
        retry_options = self._reconnection_retry_options()
        async for attempt in AsyncRetrying(**retry_options):
            with attempt:
                try:
                    logger.warning(
                        f"Attempt {attempt.retry_state.attempt_number}: \n"
                        "Trying to connect..."
                    )
                    self._connection = await asyncssh.connect(
                        host=self.host,
                        port=self.port,
                        username=self.username,
                        client_keys=ssh_keys,
                        known_hosts=str(self._knownhosts_file),
                        login_timeout=self._connection_timeout,
                        keepalive_interval=self._keepalive_interval,
                        keepalive_count_max=self._keepalive_count_max,
                        env={},
                        send_env={}
                    )
                    logger.warning(f"Connected to {self._remote_name}")
                    break
                except (asyncssh.DisconnectError, asyncssh.misc.ConnectionLost) as e:
                    logger.warning(
                        f"Connection attempt {attempt.retry_state.attempt_number} \n"
                        f"failed with error: {e}. Retrying..."
                    )
                    raise e

    def disconnect(self) -> None:
        if self._connection is not None:
            self._connection.disconnect(code=11,   # SSH_DISCONNECT_BY_APPLICATION
                                        reason="Disconnection by request of WESkit")

    # if the connection is lost, reconnect
    # no need to raise an error sice the function is part of the reconnection process
    # logger.warning should be enough for the client to know that the connection was not established
    async def _reconnect(self):
        try:
            await self.connect()
            await self.run('echo "Connection is active"')
            logger.info("Connection is active")
        except asyncssh.Error:
            logger.warning("Connection is closed")

    @property
    def raw(self) -> SSHClientConnection:
        if self._connection is None:
            raise RuntimeError("RetryableSshConnection.connect() was never called!")
        else:
            return self._connection

    def _reconnection_retry_options(self, **kwargs) -> dict:
        """
        All retries should wrap around external asynchronous calls, such as asyncssh, or
        async functions that themselves do no retry, because errors are simply re-raised.
        Thus, if the same error is causing a retry at multiple levels, retry attempts multiply!
        """
        return {**self._retry_options,
                "reraise": True,
                "retry": retry_if_exception(is_connection_interrupted),
                "after": after_log(logger, logging.DEBUG),
                **kwargs}

    @asynccontextmanager
    async def context(self, **kwargs):
        """
        A context manager that simplifies the calling of SSH operations with retry. You can add
        parameters to tenacity.AsyncRetrying to override any defaults.

        with self._retryable_connection():
            do_ssh_op()

        Exceptions raised by asyncssh are wrapped in an ExecutorError.

        Note: Retrying doesn't work inside a context manager. A context manager is only allowed
        to yield once, therefore it can't be used for multiple attempts.

        If you want to create a process with multiple attempts in case of connection problems,
        instead use the `create_process` or `run` methods.
        """
        try:
            yield self
        except asyncssh.DisconnectError as ex:
            raise DisconnectError("Disconnected") from ex
        except asyncssh.ChannelOpenError as ex:
            raise ChannelOpenError("Channel open failed") from ex
        except asyncssh.Error as ex:
            raise ExecutorError("SSH error") from ex

    @property
    def username(self) -> str:
        return self._username

    @property
    def host(self) -> str:
        return self._host

    @property
    def port(self) -> int:
        return self._port

    @property
    def keyfile(self) -> Path:
        return self._keyfile

    @property
    def _remote_name(self) -> str:
        return f"{self.username}@{self.host}:{self.port}"

    @property
    def host_url(self) -> Url:
        """
        Return the host to which an SSH connection is established to execute a command.

        * https://datatracker.ietf.org/doc/id/draft-salowey-secsh-uri-00.html
        * https://www.rfc-editor.org/rfc/rfc3986
        """
        return Url(scheme="ssh",
                   auth=self._username,
                   host=self._host,
                   port=self._port)

    async def retry_with_asyncssh(
        self,
        method: Callable[..., Awaitable[T]],
        *args,
        **kwargs
    ) -> T:
        """
        This method implements an asyncssh method call with multiple attempts in case of
        connection problems.

        :param method: The async method to call.
        :param args: Positional arguments to pass to the method.
        :param retry_options: Options for configuring retry behavior.
        :param kwargs: Keyword arguments to pass to the method.
        """
        retry_options = kwargs.pop("retry_options", {})
        async for attempt in AsyncRetrying(**self._reconnection_retry_options(**retry_options)):
            with attempt:
                try:
                    return await method(*args, **kwargs)
                except asyncssh.ConnectionLost as ex:
                    raise ex
                except asyncssh.ChannelOpenError as ex:
                    raise ex
                except asyncssh.Error as ex:
                    raise ExecutorError("SSH error") from ex
        raise ExecutorError(f"SSH {method.__name__} failed.")

    async def run(self, *args, **kwargs) -> SSHCompletedProcess:
        """
        Run a command on the remote host.
        In order to overwrite the default retry options, pass a `retry_options` dict.
        e.g. ssh_connection.run("command", retry_options=retry_options)
        """
        if self._connection is None:
            raise RuntimeError("RetryableSshConnection not initialized")
        else:
            run: SSHCompletedProcess = await self.retry_with_asyncssh(
                self._connection.run, *args, **kwargs)
            return run

    async def create_process(self, *args, **kwargs) -> SSHClientProcess:
        """
        Run a command on the remote host.
        In order to overwrite the default retry options, pass a `retry_options` dict.
        e.g. ssh_connection.create_process("command", retry_options=retry_options)
        """
        if self._connection is None:
            raise RuntimeError("RetryableSshConnection not initialized")
        else:
            process: SSHClientProcess = await self.retry_with_asyncssh(
                self._connection.create_process, *args, **kwargs)
            return process
