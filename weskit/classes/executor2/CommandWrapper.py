# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import shlex
from urllib3.util.url import Url
from typing import List, Optional, Union, cast, Dict
from pathlib import Path
from weskit.classes.ShellCommand import ShellCommand, ShellSpecial, ss
from weskit.classes.executor2.ProcessId import WESkitExecutionId
from weskit.classes.executor2.Executor import Executor
from weskit.utils import resource_path


class CommandWrapper:
    """
    Return a ShellCommand derived from the input command that allows the command to be run
    in the background and includes logging to the requested output files and of the
    exit code.

    The execution logs go into a directory `.log-{execution_id}`, unless other files
    were requested via the `stdout_url`, etc. parameters.

    Note that the stdout and stderr of the wrapper script is written to the log_dir/wrapper_stdout
    and log_dir/wrapper_stderr files.

    The -n parameter of the wrapper, attaches environment variables to the wrapped command.
    """

    def __init__(
        self,
        execution_id: WESkitExecutionId,
        command: ShellCommand,
        log_dir_base: Path,
        as_background_process: bool,
        stdout_url: Optional[Url] = None,
        stderr_url: Optional[Url] = None,
        stdin_url: Optional[Url] = None,
        env: Optional[Dict[str, str]] = None,
        api_mount_path: Optional[str] = None,
    ):
        self._execution_id = execution_id
        self._command = command
        self._log_dir_base = log_dir_base
        self._as_background_process = as_background_process
        self._stdout_url = stdout_url
        self._stderr_url = stderr_url
        self._stdin_url = stdin_url
        self._env = env
        self._api_mount_path = api_mount_path

    @property
    def execution_id(self) -> WESkitExecutionId:
        return self._execution_id

    @property
    def command(self) -> ShellCommand:
        return self._command

    @property
    def log_dir_base(self) -> Path:
        return self._log_dir_base

    @property
    def as_background_process(self) -> bool:
        return self._as_background_process

    @property
    def stdout_url(self) -> Optional[Url]:
        return self._stdout_url

    @property
    def stderr_url(self) -> Optional[Url]:
        return self._stderr_url

    @property
    def stdin_url(self) -> Optional[Url]:
        return self._stdin_url

    @property
    def env(self) -> Optional[Dict[str, str]]:
        return self._env

    @property
    def api_mount_path(self) -> Optional[str]:
        return self._api_mount_path

    def _file_url_to_path(self, url: Url) -> Path:
        if url.scheme is None:
            raise ValueError(
                "Executor only accepts file:// URLs for stdin/stdout/stderr."
            )
        elif url.scheme is not None and url.scheme != "file":
            raise ValueError(
                "Executor only accepts file:// URLs for stdin/stdout/stderr."
            )
        elif url.path is None:
            raise ValueError("Path not set for stdin/stdout/stderr.")
        return Path(url.path)

    @property
    def _wrapper_source_path(self) -> Path:
        return cast(Path, resource_path("weskit.classes.executor2.resources", "wrap"))

    def external_wrapper_source_path(self) -> Optional[Path]:
        if self.api_mount_path is not None:
            return Path(self.api_mount_path).joinpath(
                "weskit/classes/executor2/resources/wrap"
            )
        else:
            return self._wrapper_source_path

    def _env_path(self, stage_path: Path) -> Path:
        return stage_path / "env.sh"

    def _command_log_dir(self) -> Path:
        """
        Return the logging directory for the process.
        """
        if self._command.workdir is None:
            raise ValueError("Working directory has not been set.")
        return (
            self._command.workdir /
            self._log_dir_base /
            str(self._execution_id.value)
        )

    def _create_env_script(self, env_path: Path, env: Dict[str, str]) -> None:
        """
        Create an environment file with the variables requested via the command. With the script we
        get better logging, and we can ensure does not fail because of a long command line.
        """
        with open(env_path, "w") as env_fh:
            # Tag the process with `weskit_process_id` to make it
            # recoverable with a query of the environment variables
            # of process (e.g. on /proc/).
            print(
                f"{Executor.EXECUTION_ID_VARIABLE}={str(self.execution_id.value)}",
                file=env_fh,
            )
            for envvar, envval in env.items():
                print(f"export {envvar}={shlex.quote(envval)}", file=env_fh)

    def generate_wrapped_command(self) -> ShellCommand:
        if self.command.workdir is None:
            raise ValueError(
                "No workdir for command. Currently only support commands with defined workdir."
            )

        def _optional_file(param: str, url: Optional[Url]) -> List[str]:
            optional_file = []
            if url is not None:
                path = self._file_url_to_path(url)
                optional_file.append(param)
                optional_file.append(str(path))
            return optional_file

        command_log_dir = self._command_log_dir()
        wrapped_command: List[Union[str, ShellSpecial]] = list()
        # Ensure process is not killed upon shell exit.
        wrapped_command += ["mkdir", "-p", str(command_log_dir), ss(";")]
        if self.as_background_process:
            wrapped_command += ["nohup"]
        # if self.external_wrapper_source_path is not None:
        #     wrapped_command += [str(self.external_wrapper_source_path()), "-a"]
        # else:
        wrapped_command += [str(self.external_wrapper_source_path()), "-a"]
        # A working directory is mandatory!
        wrapped_command += ["-w", str(self.command.workdir)]
        wrapped_command += ["-l", str(command_log_dir)]
        if self.env is not None:
            env_path = self._env_path(stage_path=command_log_dir)
            self._create_env_script(env=self.env, env_path=env_path)
            wrapped_command += ["-n", str(env_path)]
        wrapped_command += _optional_file("-o", self.stdout_url)
        wrapped_command += _optional_file("-e", self.stderr_url)
        wrapped_command += _optional_file("-i", self.stdin_url)
        wrapped_command += ["--", self.command.command_expression]
        wrapped_command += [
            ss("1>"),
            str(command_log_dir.joinpath("wrapper_stdout")),
        ]
        wrapped_command += [
            ss("2>"),
            str(command_log_dir.joinpath("wrapper_stderr")),
        ]
        if self.as_background_process:
            wrapped_command += [ss("&")]
        return self.command.copy(command=wrapped_command)

    @property
    def command_log_dir(self) -> Path:
        return self._command_log_dir()
