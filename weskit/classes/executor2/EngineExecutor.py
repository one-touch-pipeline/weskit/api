# SPDX-FileCopyrightText: 2024 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

import logging
from typing import Optional

from asyncio import AbstractEventLoop

from weskit.classes.executor2.EngineExecutorType import EngineExecutorType
from weskit.classes.executor2.Executor import Executor

logger = logging.getLogger(__name__)


def get_executor(executor_type: EngineExecutorType,
                 login_parameters: dict,
                 event_loop: Optional[AbstractEventLoop]) -> Executor:
    """ Get the executor for the given executor type.
        Though SSHExecutor, LsfExecutor, SlurmExecutor, KubernetesExecutor
        are not ready yet.
    """
    raise NotImplementedError
