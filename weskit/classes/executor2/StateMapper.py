# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

from abc import ABCMeta, abstractmethod
from typing import Generic, TypeVar, Dict, Type

from weskit.classes.executor2.State import (
    State,
    ForeignState,
    ALLOWED_TRANSITIVE_TRANSITIONS,
    ObservedState,
    Start,
)

S = TypeVar("S")


class AbstractStateMapper(Generic[S], metaclass=ABCMeta):
    """
    Abstract class for state mappers.

    Subclasses may use simple state-code to `State` mappings, or may derive the
    `State` also from `ForeignState.reasons` or `ForeignState.exit_code`.

    Subclasses may also handle unknown foreign states in different ways, e.g. by timing out
    after some time.

    This is a `Callable`. The usage pattern is

    ```python
    mapper = MyStateMapper()
    old_state, new_state = mapper(execution_state, foreign_state)
    ```
    """

    def _check_transition(
        self,
        old_execution_state: State[S],
        suggested_execution_state: ObservedState[S],
    ):
        """
        Check whether a suggested transition is valid. Raise a RuntimeError, if that is not
        the case.
        """
        if (
            suggested_execution_state.name()
            not in ALLOWED_TRANSITIVE_TRANSITIONS[old_execution_state.name()]
        ):
            raise RuntimeError(
                f"Forbidden state transition triggered by "
                f"{suggested_execution_state.last_foreign_state.wrapped_state}: "
                f"{old_execution_state.name()} -> {suggested_execution_state.name()}"
            )

    def _transition_to_next_state(
        self,
        old_execution_state: State[S],
        suggested_execution_state: ObservedState[S],
        **kwargs,
    ) -> ObservedState[S]:
        """
        Actually transition to the new state. This boils down to closing the current state,
        and returning the previously constructed suggested `new_execution_state` as
        new state.
        """
        self._check_transition(old_execution_state, suggested_execution_state)
        old_execution_state.close(suggested_execution_state.last_foreign_state)
        return suggested_execution_state

    def _accept_foreign_state(
        self, execution_state: ObservedState[S], foreign_state: ForeignState[S]
    ) -> ObservedState[S]:
        """
        Accept the observed `foreign_state` as member of the current `execution_state`.
        """
        execution_state.add_observation(foreign_state)
        return execution_state

    @abstractmethod
    def _suggest_next_state(
        self,
        execution_state: State[S],
        foreign_state: ForeignState[S],
        *args,
        **kwargs,
    ) -> ObservedState[S]:
        """
        Suggest a next state. The possible next state is constructed, but nothing else. It may
        depend on the old `execution_state`, the observed `foreign_state`, and other contextual
        information (e.g. whether the process was sent a cancellation signal).
        """
        pass

    def _handle_unknown_foreign_state(
        self,
        execution_state: ObservedState[S],
        foreign_state: ForeignState[S],
        *args,
        **kwargs,
    ) -> ObservedState[S]:
        """
        By default, just accept unknown foreign states for as long as they persist. They are
        just added to the current state. This means, in principle wait forever for recovery
        and assume that all unknown states are actually the old state.
        """
        return self._accept_foreign_state(execution_state, foreign_state)

    def __call__(
        self,
        execution_state: State[S],
        foreign_state: ForeignState[S],
        *args,
        **kwargs,
    ) -> ObservedState[S]:
        """
        Given an `State` and an `ForeignState`, return the possibly
        modified `State` (updated), or the next `State`. The `execution_state`
        will be modified in-place (by calling `add_observation` or `close`).

        :return: new_execution_state

        This is a template method. Override the called methods to adapt the behaviour in the
        subclasses.
        """
        if isinstance(execution_state, Start):
            # The Start state is special in that it is not observable and cannot keep track of
            # observations. This means, if we observe an foreign state we always want a new
            # state.
            return self._transition_to_next_state(
                execution_state,
                self._suggest_next_state(
                    execution_state, foreign_state, *args, **kwargs
                ),
            )
        elif isinstance(execution_state, ObservedState):
            # This second isinstance() test really is only to satisfy the type inference.
            if not foreign_state.is_known:
                return self._handle_unknown_foreign_state(
                    execution_state, foreign_state, *args, **kwargs
                )
            else:
                suggested_next_state = self._suggest_next_state(
                    execution_state, foreign_state, *args, **kwargs
                )
                if execution_state.name == suggested_next_state.name:
                    return self._accept_foreign_state(execution_state, foreign_state)
                else:
                    return self._transition_to_next_state(
                        execution_state, suggested_next_state
                    )
        else:
            raise RuntimeError("Type error.")


class SimpleStateMapper(Generic[S], AbstractStateMapper[S]):
    """
    Takes a simple state-code-2-`State` mapping at construction time. This is useful
    if the next state WESkit State depends only on the `ForeignState`, but not, e.g.
    on an exit code, reason, or even more than the last `ForeignState`.
    """

    def __init__(self, state_map: Dict[S | None, Type[ObservedState[S]]]):
        """
        The state_map should map `ForeignState` instances to `State` classes. The
        classes are used to construct new `State` instances.
        """
        self.state_map = state_map

    def _suggest_next_state(
        self,
        execution_state: State[S],
        foreign_state: ForeignState[S],
        *args,
        **kwargs,
    ) -> ObservedState[S]:
        """
        This the simple strategy. Just consider the current state, and the foreign state to
        suggest a next state.
        """
        return self.state_map[foreign_state.wrapped_state].from_previous(
            execution_state, foreign_state
        )
