# SPDX-FileCopyrightText: 2023 The WESkit Contributors
#
# SPDX-License-Identifier: MIT

import logging
import asyncio
from builtins import bool, str
from pathlib import Path
from signal import Signals
from typing import Optional, cast, Dict, List
from urllib3.util.url import Url
from datetime import datetime

from kubernetes.client.api.batch_v1_api import BatchV1Api
from kubernetes.client.api.core_v1_api import CoreV1Api
from kubernetes.client.models import (
    V1Container,
    V1VolumeMount,
    V1Volume,
    V1JobSpec,
    V1PodTemplateSpec,
    V1PodSpec,
    V1Pod,
    V1ObjectMeta,
    V1Job,
    V1PersistentVolumeClaimVolumeSource,
)
from kubernetes.client.exceptions import ApiException

from weskit.classes.ShellCommand import ShellCommand
from weskit.classes.executor2.ProcessId import ProcessId
from weskit.classes.executor2.CommandWrapper import CommandWrapper
from weskit.classes.executor2.State import (
    State,
    ObservedState,
    NonTerminalState,
    TerminalState,
    Start,
    Pending,
)
from weskit.classes.executor2.ForeignState import ForeignState
from weskit.classes.executor2.Executor import (
    Result,
    Executor,
    Settings,
)
from weskit.classes.executor2.ProcessId import Identifier, WESkitExecutionId
from weskit.classes.executor2.kubernetes.KubernetesPhases import (
    KubernetesPhases,
    KubernetesStateMapper,
)
from weskit.classes.storage.StorageAccessor import StorageAccessor

logger = logging.getLogger(__name__)


class KubernetesExecutor(Executor[KubernetesPhases]):
    """
    An executor that allows you to submit jobs to Kubernetes. The submitted job is run in
    a standalone pod with the configured settings.
    """

    _state_mapper: KubernetesStateMapper = KubernetesStateMapper()

    def __init__(
        self,
        id: Identifier[str],
        k8s_batch_v1_client: BatchV1Api,
        k8s_core_v1_client: CoreV1Api,
        namespace: str,
        storage: StorageAccessor,
        wf_volume_name: str,
        wf_volume_claim_name: str,
        wf_volume_mount_path: str,
        data_volume_name: str,
        data_volume_claim_name: str,
        data_volume_mount_path: str,
        api_volume_name: str,
        api_volume_claim_name: str,
        api_volume_mount_path: str,
        log_dir_base: Optional[Path] = None,
        pod_restart_policy: Optional[str] = "Never",
    ):
        super().__init__(id, log_dir_base)
        self._k8s_batch_v1_client = k8s_batch_v1_client
        self._k8s_core_v1_client = k8s_core_v1_client
        self._namespace = namespace
        self._storage_accessor = storage
        self._pod_restart_policy = (
            "Never" if pod_restart_policy is None else pod_restart_policy
        )
        if wf_volume_name is None:
            raise ValueError("Workflow volume name for workflows needs to be set.")
        else:
            self._wf_volume_name = wf_volume_name
        if wf_volume_mount_path is None:
            raise ValueError(
                "Workflow volume mount path for workflows needs to be set."
            )
        else:
            self._wf_volume_mount_path = wf_volume_mount_path
        if wf_volume_claim_name is None:
            raise ValueError(
                "Workflow volume claim name for workflows needs to be set."
            )
        else:
            self._wf_volume_claim_name = wf_volume_claim_name

        if data_volume_name is None:
            raise ValueError("Data volume name needs to be set.")
        else:
            self._data_volume_name = data_volume_name
        if data_volume_mount_path is None:
            raise ValueError("Data volume mount path needs to be set.")
        else:
            self._data_volume_mount_path = data_volume_mount_path
        if data_volume_claim_name is None:
            raise ValueError("Data volume claim name needs to be set.")
        else:
            self._data_volume_claim_name = data_volume_claim_name

        if api_volume_name is None:
            raise ValueError("Workflow volume name for workflows needs to be set.")
        else:
            self._api_volume_name = api_volume_name
        if api_volume_mount_path is None:
            raise ValueError("API mount for weskit needs to be set.")
        else:
            self._api_volume_mount_path = api_volume_mount_path
        if api_volume_claim_name is None:
            raise ValueError("API claim name for workflows needs to be set.")
        else:
            self._api_volume_claim_name = api_volume_claim_name

    @property
    def hostname(self) -> str:
        """
        Providing default kubernetes service IP at the moment. Can be changed by passing
        the appropriate service name in the future.
        """
        return self._k8s_core_v1_client.read_namespaced_service(
            name="kubernetes", namespace=self.namespace
        )

    @property
    def namespace(self) -> str:
        """
        Namespace of the kubernetes cluster where the volumes exist
        and the jobs should run.
        """
        return self._namespace

    @property
    def k8s_batch_v1_client(self) -> BatchV1Api:
        """
        User is responsible for providing the API client to create
        and execute the jobs in the kubernetes cluster.
        """
        return self._k8s_batch_v1_client

    @property
    def k8s_core_v1_client(self) -> CoreV1Api:
        """
        User is responsible for providing the API client to
        query the job/pod status in the kubernetes cluster.
        """
        return self._k8s_core_v1_client

    @property
    def pod_restart_policy(self) -> Optional[str]:
        """
        Pod restart policy can be one of the following values: Never, Always, OnFailure
        Default value is set to Never.
        """
        return self._pod_restart_policy

    @property
    def wf_volume_name(self) -> Optional[str]:
        """
        Name of the workflow volume that specifies where the workflows exist.
        """
        return self._wf_volume_name

    @property
    def wf_volume_claim_name(self) -> Optional[str]:
        """
        Workflow PVC name that specifies the workflows volume.
        """
        return self._wf_volume_claim_name

    @property
    def wf_volume_mount_path(self) -> Optional[str]:
        """
        Workflow Volume mount path to fetch the workflows from.
        """
        return self._wf_volume_mount_path

    @property
    def data_volume_name(self) -> Optional[str]:
        """
        Name of the data volume that specifies where the results will be stored.
        """
        return self._data_volume_name

    @property
    def data_volume_claim_name(self) -> Optional[str]:
        """
        Data PVC name that specifies the data volume.
        """
        return self._data_volume_claim_name

    @property
    def data_volume_mount_path(self) -> Optional[str]:
        """
        Data Volume mount path to fetch the data from.
        """
        return self._data_volume_mount_path

    @property
    def api_volume_name(self) -> Optional[str]:
        """
        Name of the code volume that specifies where the weskit code exists.
        """
        return self._api_volume_name

    @property
    def api_volume_claim_name(self) -> Optional[str]:
        """
        Code PVC name that specifies the weskit code volume.
        """
        return self._api_volume_claim_name

    @property
    def api_volume_mount_path(self) -> Optional[str]:
        """
        Code volume mount path to fetch the weskit code from.
        """
        return self._api_volume_mount_path

    @property
    def log_dir_base(self) -> Path:
        return self._log_dir_base

    @log_dir_base.setter
    def log_dir_base(self, log_dir: Path):
        self._log_dir_base = log_dir

    def _create_container(
        self, container_name, container_image: str, command: ShellCommand
    ) -> V1Container:
        """Creates a Kubernetes V1Container object with the specified configuration.
        The container is initialized with `/bin/bash` and executes the shell command
        provided as `command.command_expression` via the `-c` argument
        """
        return V1Container(
            name=container_name,
            image=container_image,
            volume_mounts=[
                V1VolumeMount(
                    mount_path=self.wf_volume_mount_path, name=self.wf_volume_name
                ),
                V1VolumeMount(
                    mount_path=self.api_volume_mount_path, name=self.api_volume_name
                ),
                V1VolumeMount(
                    mount_path=self.data_volume_mount_path, name=self.data_volume_name
                ),
            ],
            command=["/bin/bash"],
            args=["-c", command.command_expression],
        )

    async def _generate_job(
        self, job_name: str, container_image: str, command: ShellCommand
    ) -> V1Job:
        """
        Asynchronously generates and submits a Kubernetes V1Job to the specified namespace.
        This method first constructs a Kubernetes job specification, including the pod template,
        containers, and volume mounts, and then submits the job using the Kubernetes Batch API.
        The created Kubernetes V1Job object is returned from the API server.
        """
        job_spec: V1JobSpec = V1JobSpec(
            template=V1PodTemplateSpec(
                metadata=V1ObjectMeta(name=job_name, labels={"job_name": job_name}),
                spec=V1PodSpec(
                    restart_policy=self.pod_restart_policy,
                    containers=[
                        self._create_container(
                            container_name=job_name,
                            container_image=container_image,
                            command=command,
                        )
                    ],
                    volumes=[
                        V1Volume(
                            name=self.wf_volume_name,
                            persistent_volume_claim=V1PersistentVolumeClaimVolumeSource(
                                claim_name=self.wf_volume_claim_name
                            ),
                        ),
                        V1Volume(
                            name=self.data_volume_name,
                            persistent_volume_claim=V1PersistentVolumeClaimVolumeSource(
                                claim_name=self.data_volume_claim_name
                            ),
                        ),
                        V1Volume(
                            name=self.api_volume_name,
                            persistent_volume_claim=V1PersistentVolumeClaimVolumeSource(
                                claim_name=self.api_volume_claim_name
                            ),
                        ),
                    ],
                ),
            )
        )
        job: V1Job = V1Job(
            api_version="batch/v1",
            kind="Job",
            metadata=V1ObjectMeta(name=job_name),
            spec=job_spec,
        )
        try:
            return cast(
                V1Job,
                self.k8s_batch_v1_client.create_namespaced_job(
                    body=job, namespace=self.namespace
                ),
            )
        except ApiException:
            logger.error(f'Failed to create Kubernetes job "{job_name}."')
            raise

    async def _get_states_dict(
        self, current_run_states: List[State[KubernetesPhases]]
    ) -> Dict[WESkitExecutionId, ForeignState[KubernetesPhases]]:
        """
        Get the states of all processes in the kubernetes pods.
        For each job retrieve the most important information,
        such as job ID, job status, exit code, exit reason.
        If the state could not be retrieved, return status None for this process.
        """
        try:
            job_list = self.k8s_batch_v1_client.list_namespaced_job(
                namespace=self.namespace
            )
        except ApiException as e:
            logger.error(f"Failed to get job states: {str(e)}")
            return {}
        job_names_cluster = [job.metadata.name for job in job_list.items]
        job_ids = [run_state.execution_id for run_state in current_run_states]
        foreign_states = {}
        for job_id in job_ids:
            job_name = str(job_id.value).lower()
            # check phase for not terminated jobs in database
            if job_name not in job_names_cluster:
                raise Exception(f"Job {job_name} not found in the kubernetes cluster.")
            pod_phase = self.get_pod_phase_by_job_name(job_name)
            current_foreign_state = KubernetesPhases.as_foreign_state(
                pid=ProcessId(job_name, "kubernetes"),
                name=KubernetesPhases.from_phase_name(pod_phase).name,
            )
            foreign_states[job_id] = current_foreign_state
        return foreign_states

    async def execute(
        self,
        execution_id: WESkitExecutionId,
        command: ShellCommand,
        stdout_file: Optional[Url] = None,
        stderr_file: Optional[Url] = None,
        stdin_file: Optional[Url] = None,
        settings: Optional[Settings] = None,
        **kwargs,
    ) -> State[KubernetesPhases]:
        """
        Submit a command to the kubernetes cluster through a job. Use the
        WeskitExecutionID as an identifier for the job. Please note that
        k8s does not allow upper case letters as job/pod name, hence we
        convert the uppercase letters to lowercase letters before using
        it as the job name.
        """
        start_state: State = Start(execution_id)
        wrapperCommand: CommandWrapper = CommandWrapper(
            execution_id=execution_id,
            command=command,
            log_dir_base=self.log_dir_base,
            as_background_process=False,
            api_mount_path=self.api_volume_mount_path,
            stderr_url=stderr_file,
            stdin_url=stdin_file,
            stdout_url=stdout_file,
        )
        wrappedCommand: ShellCommand = wrapperCommand.generate_wrapped_command()
        #  overwrite log_dir_base with the one from the wrapperCommand
        self.log_dir_base = wrapperCommand.command_log_dir

        if settings is None or settings.container_image is None:
            raise ValueError(
                "Cannot execute the command. Container image not specified"
            )
        else:
            _: V1Job = await self._generate_job(
                job_name=str(execution_id.value).lower(),
                container_image=settings.container_image,
                command=wrappedCommand,
            )
            execution_state: State = Pending(
                execution_id=execution_id,
                foreign_state=KubernetesPhases.as_foreign_state(
                    pid=ProcessId(str(execution_id.value), "kubernetes"),
                    name="Pending",
                ),
                previous_state=start_state,
            )
        return execution_state

    def get_pod_phase_by_job_name(self, job_name: str) -> Optional[str]:
        """
        Retrieves the phase (status) of the first pod associated with a given
        Kubernetes Job name.
        """
        try:
            pod_list = self.k8s_core_v1_client.list_namespaced_pod(
                namespace=self.namespace, label_selector=f"job_name={job_name}"
            )
            pod: V1Pod = cast(
                V1Pod,
                self.k8s_core_v1_client.read_namespaced_pod(
                    name=pod_list.items[0].metadata.name, namespace=self.namespace
                ),
            )
            return pod.status.phase
        except ApiException as e:
            logger.error(f'Failed to get pod phase for job "{job_name}": {str(e)}')
            return None

    async def get_status(
        self,
        execution_id: WESkitExecutionId,
        log_dir: Url | None = None,
        pid: int | None = None,
    ) -> Optional[State[KubernetesPhases]]:
        return await self.update_status(
            current_state=Start(execution_id), log_dir=log_dir, pid=pid
        )

    async def update_status(
        self,
        current_state: State[KubernetesPhases],
        log_dir: Url | None = None,
        pid: int | None = None,
    ) -> Optional[State[KubernetesPhases]]:
        job_name: str = str(current_state.execution_id)
        pod_phase: Optional[str] = self.get_pod_phase_by_job_name(job_name)
        current_foreign_state = KubernetesPhases.as_foreign_state(
            pid=ProcessId(job_name, "kubernetes"),
            name=KubernetesPhases.from_phase_name(pod_phase).name,
        )
        return KubernetesExecutor._state_mapper(
            execution_state=current_state, foreign_state=current_foreign_state
        )

    async def get_result(
        self,
        state: State[KubernetesPhases],
        shell_command: ShellCommand,
        execution_log: Dict,
    ) -> Result[KubernetesPhases]:
        """
        Retrieve the result of a single job, including standard output and error streams.
        The result also includes the exit code if the job has completed.

        :param state: The current state of the job.
        :return: An instance of Result containing stdout, stderr, and optionally the exit code.
        """
        # Get the job status
        job_name = str(state.execution_id.value).lower()
        pod_phase: Optional[str] = self.get_pod_phase_by_job_name(job_name)
        current_foreign_state = KubernetesPhases.as_foreign_state(
            pid=ProcessId(job_name, "kubernetes"),
            name=KubernetesPhases.from_phase_name(pod_phase).name,
        )

        observed_state: ObservedState
        if pod_phase not in ["Succeeded", "Failed"]:
            observed_state = NonTerminalState(
                state.execution_id, current_foreign_state, state
            )
        else:
            observed_state = TerminalState(
                state.execution_id, current_foreign_state, state
            )

        # Build the Result object
        result: Result = Result(
            command=shell_command,
            stdout_url=Url(execution_log["stdout_file"]),
            stderr_url=Url(execution_log["stderr_file"]),
            stdin_url=None,
            state=observed_state,
            start_time=execution_log["start_time"],
            end_time=None,
        )

        exit_code = None
        if pod_phase == "Succeeded":
            exit_code = 0
        elif pod_phase == "Failed":
            exit_code = 1
        # Set exit code if job is completed
        if exit_code is not None:
            result.end_time = datetime.now()  # Update end time if job is completed

        return result

    async def get_states(
        self,
        execution_ids: List[WESkitExecutionId],
    ) -> List[State[KubernetesPhases]]:
        """
        Get the states of all processes in the kubernetes pods.
        For each job retrieve the most important information,
        such as job ID, job status, exit code, exit reason.
        If the state could not be retrieved for a specific process, ignore it.
        """
        states: List[State[KubernetesPhases]] = [
            Start(execution_id) for execution_id in execution_ids
        ]
        return await self.update_states(current_run_states=states)

    async def update_states(
        self, current_run_states: List[State[KubernetesPhases]]
    ) -> List[State[KubernetesPhases]]:
        """
        Update the states of all processes if possible.
        If the state update for a certain job fails, ignore it.
        """
        foreign_states = await self._get_states_dict(current_run_states)
        if len(foreign_states) != len(current_run_states):
            raise Exception("Failed to get kubernetes phases for all unfinished jobs.")
        updated_states: List[State[KubernetesPhases]] = []
        for current_run_state in current_run_states:
            id = current_run_state.execution_id
            updated_state = KubernetesExecutor._state_mapper(
                execution_state=current_run_state, foreign_state=foreign_states[id]
            )
            updated_states.append(updated_state)
        return updated_states

    async def wait(self, state: State[KubernetesPhases]) -> None:
        job_name: str = str(state.execution_id.value).lower()

        while True:
            pod_phase = self.get_pod_phase_by_job_name(job_name)
            if pod_phase in ["Succeeded", "Failed", "Unknown", None]:
                break
            await asyncio.sleep(10)

    async def kill(self, state: State[KubernetesPhases], signal: Signals) -> bool:
        job_name: str = str(state.execution_id.value).lower()

        try:
            self.k8s_batch_v1_client.delete_namespaced_job(
                name=job_name,
                namespace=self.namespace,
                body={"propagationPolicy": "Foreground"},
            )
            return True
        except ApiException as e:
            if e.status == 404:
                logger.warning(
                    f'Job "{job_name}" could not be killed, because it was not found.'
                )
                return False
            else:
                raise

    @property
    def storage(self) -> StorageAccessor:
        return self._storage_accessor
